﻿using System.Diagnostics;
using System.Text;

var queens = new List<(int x, int y)>();
var solutions = new List<List<(int x, int y)>>();
const int boardSize = 8;

void CopyToSolutions()
{
    // We are using a mutable storage, so wee need to copy it to prevent mutation of the found solutions
    var copy = new (int, int)[boardSize];
    queens.CopyTo(copy);
    solutions.Add(copy.ToList());
}

void PrintSolution(List<(int x, int y)> solution)
{
    var board = new StringBuilder();
    board.Append("\n");
    for (var row = 1; row <= boardSize; row++)
    {
        for(var column = 1; column <= boardSize; column++)
        {
            board.Append(solution.Any(queen => queen.x == column && queen.y == row) ? "X " : "- ");
        }
        board.Append("\n");
    }
    board.Append("\n");
    Console.WriteLine(board.ToString());
}

void PlaceNewQueen()
{
    if (queens.Count == boardSize)
    {
        CopyToSolutions();
        return;
    }

    var row = queens.Count + 1;
    
    for (var column = 1; column <= boardSize; column++)
    {
        if(queens.Any(queen => queen.x == column))
           continue;

        if(queens.Any(queen => Math.Abs(row - queen.y) == Math.Abs(column - queen.x)))
            continue;

        queens.Add((column, row));
        PlaceNewQueen();
        queens.RemoveAt(queens.Count - 1);
    }
}

Console.WriteLine("Calculating ..");

var stopWatch = Stopwatch.StartNew();
PlaceNewQueen();
stopWatch.Stop();

Console.WriteLine($"Done. Found {solutions.Count} solutions in {stopWatch.Elapsed}.");
Console.WriteLine("Print solutions? (y/n)");
var input = Console.ReadKey();
if(input.KeyChar == 'y')
    foreach (var solution in solutions)
        PrintSolution(solution);